# Custom Trig Functions - Java

Assignment 2 part 2 for COMP-4110 class at the University of Windsor


**Brief Writeup on Testing and Version Iterations**

Testing:
- All test cases were decided on by researching unique values that are specifc to all three of the functions (sin, cos, tan).
    These resulted in cases for:
        - each of the 4 quadrants depicted when drawing a circle on a graph. (12 cases)
        - edge case for a complete circle (2PI or 360 Degrees) (3 cases)
        - and the sin, cos, and tan of zero (3 cases)  
    other cases were simple:
        - using a negative value (3 cases)
        - the degree version of all the other (21 cases) 

Version 1.0:
- In this version I took the advice of the assignment posting and created just the framework for all six functions 
theses functions were for calculating sin, cos, and tan as well as doing the conversion from degrees for each.
- This version for obvious reasons, resulted in 38 failed tests out of 42 with only a couple random test cases working by returning the input value as the calculated result.

Version 2.0:
- In this version I added in the logic for doing the conversions as well as the base for calculating the taylor series expansions for sin and cos.
- The logic for calculating tan was also included but is dependant on the correctness of the sin and cos functions
- This version resulted in 37 failed tests which barely improved upon the first version. This is due to issues with the logic for sin and cos.

Version 3.0:
- In this version I added in checks for valid input numbers and the period in which the functions for sin and cos were in to determine the sign of the result.
- The logic was added to both sin and cos, however only the cos function had the correct term and result initial values.
- This version resulted in only 18 failed tests which is just under half of all test cases. This is thanks in part ot the cos function tests passing

Version 4.0:
- In the final version, the correct values for the term and result's initial values were used and removed all issues with version 3.0.
- This version resulted in a perfect 42 out of 42 test cases passed.