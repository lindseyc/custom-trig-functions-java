// Christopher Lindsey
// 104384505
// Assignment 2 Part 2
// Due Date: February 24th, 11:59pm

package trigFunctions;

// Class that contains the 3 trig functions and the 3 functions for converting them from radians to degrees
public class TrigFunctions {
	
	// Default constructor
	public TrigFunctions() {}
	
	// MyTrigFunctions class
	public static class MyTrigFunctions {
		
		// Default constructor
		public MyTrigFunctions() {}
		
		// Establishing the value for Pi (first 20 decimal places of pi)
	    public final static double pi = 3.14159265358979323846;
	    
	    // ------------------
	    // Calculating Sin(x) 
	    // ------------------
	    
	    public static double Sin (double x) {
	    	
	    	// Term represents base^exp / exp for taylor series
	    	// Result represents the result of all terms up to 'i' in taylor series
	    	double term = 1;
	        double result  = 0;
			
			// Checking that the number is valid to apply the Sine function to it
			if (x == Double.NEGATIVE_INFINITY || !(x < Double.POSITIVE_INFINITY)) {
		        return Double.NaN;
			}

		    // Setting the period for sine
		    x %= 2 * pi;
		              
		    // calculation for sin taylor series
	        for (int i = 1; term != 0; i++) {

	            term *= (x / i);

	            // Checking for quadrant 2
	            if (i % 4 == 1) {
	            	result += term;
	            }

	            // Checking for quadrant 4
	            if (i % 4 == 3) {
	            	result -= term;
	            }
	        }

		    return result;
		   
		}
	    
	    // ------------------
	    // Calculating Cos(x) 
	    // ------------------
	    
	    public static double Cos (double x) {
	    	
	    	double term = 1;
	        double result  = 1;
			
			if (x == Double.NEGATIVE_INFINITY || !(x < Double.POSITIVE_INFINITY)) {
		        return Double.NaN;
			}
		    x %= 2 * pi;
		              
		    // calculation for cos taylor series
	        for (int i = 1; term != 0; i++) {
	        	
	            term *= (x / i);

	            // Checking for quadrant 1
	            if (i % 4 == 0) {
	            	result += term;
	            }
	            
	            // Checking for quadrant 3
	            if (i % 4 == 2) {
	            	result -= term;
	            }
	        }

		    return result;
		   
		}
	    
	    // ------------------
	    // Calculating Tan(x) 
	    // ------------------
	    
	    public static double Tan (double x) {
	    
	    	double result;
	    	
	    	result = Sin(x) / Cos(x);
	    	
	    	return result;
	    	
	    }
	    
	    // ---------------------------
	    // Sin to Degrees Calculation 
	    // ---------------------------
	    
	    public static double sin_to_degrees(double x) {
	    	x *= (pi/180);
	    	return Sin(x);
	    }
	    
	    // ---------------------------
	    // Cos to Degrees Calculation 
	    // ---------------------------
	    
	    public static double cos_to_degrees(double x) {
	    	x *= (pi/180);
	    	return Cos(x);
	    }
	    
	    // ---------------------------
	    // Tan to Degrees Calculation 
	    // ---------------------------
	    
	    public static double tan_to_degrees(double x) {
	    	x *= (pi/180);
	    	return Tan(x);
	    }
	}
}
