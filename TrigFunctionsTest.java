package trigFunctions;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Before;
import trigFunctions.TrigFunctions.MyTrigFunctions;
import java.lang.Math;
import org.junit.jupiter.api.Test;

public class TrigFunctionsTest {

	public TrigFunctions.MyTrigFunctions trig;		
	
	// Establishing the value for Pi (first 20 decimal places of pi)
    public final static double pi = 3.14159265358979323846;
    
    @Before
    public void configuration() throws Exception {
    	trig = new TrigFunctions.MyTrigFunctions();
    }
    
    // For testing we will compare our custom functions against the Math library counterparts to compare 
    // correctness. Since our functions use a set value for pi and our calculation of the taylor series
    // will be less accurate than that of the Masth library, we will use a leeway value of 0.05.
    
    /*
    The following tests will be conducted:
    - Testing a negative number (Sin, Cos, Tan)
    - Testing zero (Sin, Cos, Tan)
    - Testing an edge case (Sin, Cos, Tan)
    - Testing all four quadrants (Sin, Cos, Tan)
    - Repeating all above test cases but using degrees functions instead of radians
    */
	
	// ---------------------------
    // Negative Number Test Cases
    // ---------------------------
    
	@Test
	public void negativeTestSin() {
		double x = -2.4;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void negativeTestCos() {
		double x = -2.4;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void negativeTestTan() {
		double x = -2.4;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void negativeTestDegreesSin() {
		double x = -60;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void negativeTestDegreesCos() {
		double x = -60;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void negativeTestDegreesTan() {
		double x = -60;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
	// ----------------
    // Zero Test Cases
    // ----------------
    
	@Test
	public void zeroTestSin() {
		double x = 0;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void zeroTestCos() {
		double x = 0;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void zeroTestTan() {
		double x = 0;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void zeroTestDegreesSin() {
		double x = 0;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void zeroTestDegreesCos() {
		double x = 0;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void zeroTestDegreesTan() {
		double x = 0;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}

	// ----------------
    // Edge Test Cases
    // ----------------
    
	@Test
	public void edgeTestSin() {
		double x = 2 * pi;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void edgeTestCos() {
		double x = 2 * pi;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void edgeTestTan() {
		double x = 2 * pi;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void edgeTestDegreesSin() {
		double x = 360;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void edgeTestDegreesCos() {
		double x = 360;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void edgeTestDegreesTan() {
		double x = 360;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
	// --------------------
    // First Quadrant Cases
    // --------------------
    
	@Test
	public void firstQuadrantTestSin() {
		double x = 1.4;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void firstQuadrantTestCos() {
		double x = 1.4;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void firstQuadrantTestTan() {
		double x = 1.4;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void firstQuadrantDegreesSin() {
		double x = 60;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void firstQuadrantDegreesCos() {
		double x = 60;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void firstQuadrantDegreesTan() {
		double x = 60;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
	// ----------------------
    // Second Quadrant Cases
    // ----------------------
    
	@Test
	public void secondQuadrantTestSin() {
		double x = 2.4;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void secondQuadrantTestCos() {
		double x = 2.4;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void secondQuadrantTestTan() {
		double x = 2.4;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void secondQuadrantDegreesSin() {
		double x = 140;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void secondQuadrantDegreesCos() {
		double x = 140;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void secondQuadrantDegreesTan() {
		double x = 140;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
	// --------------------
    // Third Quadrant Cases
    // --------------------
    
	@Test
	public void thirdQuadrantTestSin() {
		double x = 3.1;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void thirdQuadrantTestCos() {
		double x = 3.1;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void thirdQuadrantTestTan() {
		double x = 3.1;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void thirdQuadrantDegreesSin() {
		double x = 250;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void thirdQuadrantDegreesCos() {
		double x = 250;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void thirdQuadrantDegreesTan() {
		double x = 250;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
	// ---------------------
    // Fourth Quadrant Cases
    // ---------------------
    
	@Test
	public void fourthQuadrantTestSin() {
		double x = 5.6;
		double result = MyTrigFunctions.Sin(x);
		assertEquals(Math.sin(x), result, 0.05);
	}
	@Test
	public void fourthQuadrantTestCos() {
		double x = 5.6;
		double result = MyTrigFunctions.Cos(x);
		assertEquals(Math.cos(x), result, 0.05);
	}
	@Test
	public void fourthQuadrantTestTan() {
		double x = 5.6;
		double result = MyTrigFunctions.Tan(x);
		assertEquals(Math.tan(x), result, 0.05);
	}
	@Test
	public void fourthQuadrantDegreesSin() {
		double x = 330;
		double result = MyTrigFunctions.sin_to_degrees(x);
		assertEquals(Math.sin(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void fourthQuadrantDegreesCos() {
		double x = 330;
		double result = MyTrigFunctions.cos_to_degrees(x);
		assertEquals(Math.cos(Math.toRadians(x)), result, 0.05);
	}
	@Test
	public void fourthQuadrantDegreesTan() {
		double x = 330;
		double result = MyTrigFunctions.tan_to_degrees(x);
		assertEquals(Math.tan(Math.toRadians(x)), result, 0.05);
	}
	
}
